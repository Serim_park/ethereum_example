// To compile, we need solidicy compiler
// Install via npm install --save solc

const path = require('path');
const fs = require('fs');
const solc = require('solc')


const inboxPath = path.resolve(__dirname, 'contracts', 'Inbox.sol');
const source = fs.readFileSync(inboxPath, 'utf8');


// contracts[:'Inbox'] has two properties
// -- interface: Javascript abi
// -- bytecode: actual raw compiled contract

module.exports = solc.compile(source, 1).contracts[':Inbox'];
//console.log(module.exports);
//compile via -- node compile.js
