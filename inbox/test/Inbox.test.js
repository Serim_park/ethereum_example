// Add node libraries
// npm install --save mocha ganache-cli web3@1.0.0-beta.26

const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');

const provider = ganache.provider();
const web3 = new Web3(provider);

// const web3 = new Web3(ganache.provider());
const { interface, bytecode } = require('../compile');

let accounts;  // list of accounts
let inbox; // deployed contract

beforeEach( async () =>{
	// Get a list of all accounts

	// Asyncronous. Retrieve promises. 
	// web3.eth.getAccounts()
	// 	.then(fetchedAccounts =>{
	// 		console.log(fetchedAccounts);
	// 	})

	// we are going to use await instead of promises. 
	// wait until the request is completed. async flag is needed in declaration
	accounts = await web3.eth.getAccounts();
	// Use one of those accounts to deploy the contract

	// constructor function. create the instance of contract	
	// the contract is not fully created
	// until it is configured with `.send` 	
	// i.e. there is no inbox.options.address 
	inbox = await new web3.eth.Contract(JSON.parse(interface))
		.deploy({data: bytecode, arguments: ['dummy arguments']})		
		.send({from: accounts[0], gas:'1000000'}); // first account				

	inbox.setProvider(provider);
});

describe('Inbox', () =>{
	it('Test0: contructs a contract', ()=>{
		console.log(inbox);
	});
	it('Test1: creates a contract', () => {		
		 assert.ok(inbox.options.address); // address exists
	});
	it('Test2: default message', async () => {
		// calling a method is async
		// calling a function -- we don't modify the data
		const message = await inbox.methods.message().call();		
		assert.equal(message, 'dummy arguments')
	});

	it('Test3: set message', async ()=>{
		// sending a transaction
		// we modify the transaction data --> needs gas 
		await inbox.methods.setMessage('I am tired...').send({from:accounts[0]});	
		const message = await inbox.methods.message().call();		
		assert.equal(message, 'I am tired...')
	});

});

// class Car {
// 	park() {
// 		return 'stopped';
// 	}
// 	drive(){
// 		return 'vroom';
// 	}
// }


// let car; // car need to be global so that it can be accessed in describe scope
// beforeEach(() => {
// 	console.log('Initialization');
// 	car = new Car; 
// });

// describe('class Car test suit', () => {
// 	it('Test park()', ()=>{
// 		//const car = new Car();
// 		assert.equal(car.park(),'stopped');
// 	});	
// 	it('Test drive()', ()=>{
// 		//const car = new Car();
// 		assert.equal(car.drive(),'vroom');
// 	});	
// })

// //package.json <-- test :"mocha"
// //npm run test



