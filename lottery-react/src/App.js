import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import web3 from './web3';
import lottery from './lottery';

class App extends Component {

  // constructor to intialize state object
  // constructor(props){
  //   super(props);
  //   this.state = {manager: ''};
  // }
  state = { 
    manager:'',
    players: [],
    balance: '',
    value: '',
    message: '',
    contract_address: '',
    winner: []
  };

  async componentDidMount(){    
    // when we are using metamask provider 
    // we don't have to specify the from:accounts[0] arguments
    // it is configured in default as the main account in metamask
    const manager = await lottery.methods.manager().call();
    const players = await lottery.methods.getPlayers().call();

    const contract_address = await lottery.options.address;
    // this object is not a number, and therefore the balance is initialized
    // as empty string 
    const balance = await web3.eth.getBalance(contract_address);  

    this.setState({manager, players, balance, contract_address});
   
  }

  // event handler
  onSubmit = async (event) => {
    
    this.setState({message: 'Submitted!'});

    event.preventDefault();

    const accounts = await web3.eth.getAccounts();

    this.setState({message: 'Waiting on transaction success..'});

    await lottery.methods.enter().send({
      from: accounts[0],
      value: web3.utils.toWei(this.state.value, 'ether')
    });

    this.setState({message: 'You have been entered!'});

  };

  onClick = async (event) => {
    
    this.setState({message: "Selecting a winner.."})    
    const accounts = await web3.eth.getAccounts();
    const winner = await lottery.methods.pickWinner().send({
      from:accounts[0],      
    });
    
    //this.setState({winner});
    this.setState({message: 'The winner is selected!'});

  }

  render() {
    // promises. cannot use await with render.
    web3.eth.getAccounts().then(console.log);    

    // console.log(web3.version);
    return (      
        <div>
          <h2>Lottery Contract</h2>
          <p>This contract is managed by {this.state.manager}. <br/>
          The contract is deployed at {this.state.contract_address}. <br/>

          There are currently {this.state.players.length} perople entered
          competing to win {web3.utils.fromWei(this.state.balance, 'ether')} ether!

          Currently entered players: <br/>
          {this.state.players}
          </p>
          <hr/>
          <form onSubmit={this.onSubmit}>
          <h4>Want to try your luck?</h4>
          <div>
            <label>Amount of ether to enter: </label>
            <input
              value = {this.state.value}
              onChange ={event => this.setState({value: event.target.value})}            
            />
          </div>
          <button>Enter</button>
          </form>
          <hr/>
          <h4>Ready to pick a winner?</h4>
          <button onClick={this.onClick}> Pick a winner!</button>
          <hr/>
          <h1>{this.state.message}</h1>   
          <p>{this.state.winner}</p>       
        </div>
    );
  }
}

export default App;
