import Web3 from 'web3';

// window.web3 : copy of metamask web3
// currentProvider: preconfigured to connect to Rinkeby network
 const web3 = new Web3(window.web3.currentProvider);

// preconfigured web3 1.0.0 can be now used through out the app
 export default web3; 

