// To compile, we need solidicy compiler
// Install via npm install --save solc

const path = require('path');
const fs = require('fs');
const solc = require('solc')


const lotteryPath = path.resolve(__dirname, 'contracts', 'Lottery.sol');
const source = fs.readFileSync(lotteryPath, 'utf8');


// contracts[:'Inbox'] has two properties
// -- interface: Javascript abi
// -- bytecode: actual raw compiled contract

module.exports = solc.compile(source, 1).contracts[':Lottery'];
//console.log(module.exports);
//compile via -- node compile.js
