pragma solidity ^0.4.17;

contract Lottery{
    address public manager;
    address[] public players;
    // uint [] public entryMoney;
    
    // constructor
    // address of the one who creates the Lottery contract
    // will be stored in manager variable 
    // function Lottery() public{
    //     manager = msg.sender;
    // }
    constructor() public{
        // msg is a global variable 
        // always indicates the entity creating the transaction  
        manager = msg.sender;
    }
    function enter() public payable{
        // 0.01 ether will be converted to wei
        require(msg.value >.01 ether);
        players.push(msg.sender);
        // entryMoney.push(msg.value);
    }
    
    function random() private view returns (uint) {
        // sha3 is global function
        // block is global variable 
        // now is global variable -- time 
        return uint(sha3(block.difficulty, now, players));
    }
    
    function getNumplayers() public view returns (uint){
        return players.length;
    }
    
    function pickWinner() public  view returns (address) {
        // check whomever calling the function is the manager
        require(msg.sender == manager);
        // pick a psudo-random winner
        uint index = random() % players.length;
        // addresses are more than a number, they have methods tied to them 
        // this --> instance of current contract
        
        address winner = players[index];
        players[index].transfer(this.balance);         
        // initialize players with 0-length array
        players = new address[](0);
        return winner;
    }
    
    // create a function modifier
    // `manager_can_only_call` modifier in function now 
    // enforces the require statement 
    modifier manager_can_only_call(){
        require(msg.sender == manager);
        _; // replacement where the functions with this modifier will be copied 
    }
    
    // function returnEntries() public manager_can_only_call {
        
    // }
    
    function getPlayers() public view returns (address[]){
        return players;
    }
}