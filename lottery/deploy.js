//npm install --save truffle-hdwallet-provider
const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const assert = require('assert');
const {interface, bytecode} = require('./compile');

const provider = new HDWalletProvider(
	// metamask mneumonic
	'open earn dentist dune end twelve force fame proof cruise kitchen burger',
	// url of what network we are going to connect to
	// rinkeby test endpoint provided by Infura API
	'https://rinkeby.infura.io/W6f0gGsR8BHpCnUz7fio'
	);
const web3 = new Web3(provider);

// write a function to use await syntex
// because await can be only used inside a function scope 
const deploy = async () => {
	// get list of unlocked accounts
	const accounts = await web3.eth.getAccounts();
	
	// accounts[0] will return my metamask rinkeby account 
	console.log('attempting to deploy from account', accounts[0]);
	
	const result = await new web3.eth.Contract(JSON.parse(interface))
		// without '0x' it will throw error 
	 	.deploy({data: '0x'  + bytecode})
		// make sure that the gas value is in string
		.send({from: accounts[0], gas: '1000000'})
		.catch(err => {
    		console.log("Contract ERROR: ", err);
    		process.exit()
    	});

		
	// result.setProvider(provider);
	console.log(interface);
	console.log('Contract deployed to the address', result.options.address);


};

deploy();

// run with node deploy.js
// attempting to deploy from account 0xA42c0c728f03b5Cde6BCb7374C21700C7D845F72
// Contract deployed to the address 0x3495b907aFE271A413488E13b85365aA4E0cd91B
