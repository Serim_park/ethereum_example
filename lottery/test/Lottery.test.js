// Add node libraries
// npm install --save mocha ganache-cli web3@1.0.0-beta.26

const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');


const provider = ganache.provider();
const web3 = new Web3(provider);

// //get ABI of contract and raw compile contract
// // curly brace for requiring object
const { interface, bytecode } = require('../compile');

// // global variables 
let accounts;  // list of accounts
let lottery; // deployed contract


beforeEach(async () =>{
	// Get a list of all accounts
	accounts = await web3.eth.getAccounts();
	// Call the Contract constructor to initialize the contract
	// and use deloy/send method to actually create the contract
	// one who creates the transaction has to pay the gas
	lottery = await new web3.eth.Contract(JSON.parse(interface))
		.deploy({data: bytecode})
		.send({from:accounts[0], gas:'1000000'});

	lottery.setProvider(provider);	
});



describe('Lottery contract', ()=>{
	it('deploys a contract', ()=>{
		//console.log(lottery);
		assert.ok(lottery.options.address);
	});	

	it('allows one account to enter ', async() => {
		await lottery.methods.enter().send({
			from: accounts[0],
			value: web3.utils.toWei('0.02')
		});
		const players = await lottery.methods.getPlayers().call({
			from: accounts[0]
		});
		assert.equal(accounts[0], players);
		assert.equal(1, players.length);
	});

	it('allows multiple accounts to enter', async()=>{
		await lottery.methods.enter().send({
			from: accounts[0],
			value: web3.utils.toWei('0.02')
		});
		await lottery.methods.enter().send({
			from: accounts[1],
			value: web3.utils.toWei('0.02')
		});
		const players = await lottery.methods.getPlayers().call({
			from: accounts[0]
		});
		assert.equal(accounts[0], players[0]);
		assert.equal(accounts[1], players[1]);
		assert.equal(2, players.length); 

	});

	// this test will pass even value > 0.01 ether. 
	// how to make it not pass?
	it('requires a minimum amount of ether to enter', async()=>{
		try {
			await lottery.method.enter().send({
				from: accounts[0],
				value: 200
			});	
			assert(false) // throws an error 
		} catch (err){
		// catch that error is present					
			assert(err);		
		}		
	});

	it('only manager can call pickwinner',async()=>{
		try{
			await lottery.methods.pickWinner().send({
				from: accounts[1]
			}); 
			assert(false);
		} catch(err){
			assert(err);
		}
	});

	it('sends money to the winner and resets',async()=>{
		await lottery.methods.enter().send({
			from: accounts[0],
			value: web3.utils.toWei('2')			
		});
		const initialBalance = await web3.eth.getBalance(accounts[0]);
		await lottery.methods.pickWinner().send({
			from: accounts[0]
		});
		const finalBalance = await web3.eth.getBalance(accounts[0]);

		const difference = finalBalance - initialBalance;
		console.log('The gas spent is', web3.utils.toWei('2') - difference, 'wei');
		assert(difference > web3.utils.toWei('1.8'));
	});
});











